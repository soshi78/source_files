﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblDisplay = New System.Windows.Forms.Label()
        Me.lbxLap = New System.Windows.Forms.ListBox()
        Me.lbxSplit = New System.Windows.Forms.ListBox()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btmStop = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnLap = New System.Windows.Forms.Button()
        Me.btnSplit = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'lblDisplay
        '
        Me.lblDisplay.BackColor = System.Drawing.Color.White
        Me.lblDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDisplay.Font = New System.Drawing.Font("メイリオ", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblDisplay.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblDisplay.Location = New System.Drawing.Point(35, 20)
        Me.lblDisplay.Name = "lblDisplay"
        Me.lblDisplay.Size = New System.Drawing.Size(323, 53)
        Me.lblDisplay.TabIndex = 0
        Me.lblDisplay.Text = "8888:88:88.8"
        Me.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbxLap
        '
        Me.lbxLap.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbxLap.FormattingEnabled = True
        Me.lbxLap.ItemHeight = 24
        Me.lbxLap.Location = New System.Drawing.Point(26, 124)
        Me.lbxLap.Name = "lbxLap"
        Me.lbxLap.Size = New System.Drawing.Size(222, 52)
        Me.lbxLap.TabIndex = 1
        '
        'lbxSplit
        '
        Me.lbxSplit.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbxSplit.FormattingEnabled = True
        Me.lbxSplit.ItemHeight = 24
        Me.lbxSplit.Location = New System.Drawing.Point(26, 207)
        Me.lbxSplit.Name = "lbxSplit"
        Me.lbxSplit.Size = New System.Drawing.Size(222, 52)
        Me.lbxSplit.TabIndex = 2
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(273, 95)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(85, 28)
        Me.btnStart.TabIndex = 3
        Me.btnStart.Text = "start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btmStop
        '
        Me.btmStop.Location = New System.Drawing.Point(273, 129)
        Me.btmStop.Name = "btmStop"
        Me.btmStop.Size = New System.Drawing.Size(85, 28)
        Me.btmStop.TabIndex = 4
        Me.btmStop.Text = "stop"
        Me.btmStop.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(273, 163)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(85, 28)
        Me.btnReset.TabIndex = 5
        Me.btnReset.Text = "reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnLap
        '
        Me.btnLap.Location = New System.Drawing.Point(273, 197)
        Me.btnLap.Name = "btnLap"
        Me.btnLap.Size = New System.Drawing.Size(85, 28)
        Me.btnLap.TabIndex = 6
        Me.btnLap.Text = "lap"
        Me.btnLap.UseVisualStyleBackColor = True
        '
        'btnSplit
        '
        Me.btnSplit.Location = New System.Drawing.Point(273, 231)
        Me.btnSplit.Name = "btnSplit"
        Me.btnSplit.Size = New System.Drawing.Size(85, 28)
        Me.btnSplit.TabIndex = 7
        Me.btnSplit.Text = "split"
        Me.btnSplit.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.Location = New System.Drawing.Point(94, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 28)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "LAP"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(94, 176)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 28)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "SPLIT"
        '
        'Timer1
        '
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 277)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSplit)
        Me.Controls.Add(Me.btnLap)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btmStop)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.lbxSplit)
        Me.Controls.Add(Me.lbxLap)
        Me.Controls.Add(Me.lblDisplay)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblDisplay As Label
    Friend WithEvents lbxLap As ListBox
    Friend WithEvents lbxSplit As ListBox
    Friend WithEvents btnStart As Button
    Friend WithEvents btmStop As Button
    Friend WithEvents btnReset As Button
    Friend WithEvents btnLap As Button
    Friend WithEvents btnSplit As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Timer1 As Timer
End Class
